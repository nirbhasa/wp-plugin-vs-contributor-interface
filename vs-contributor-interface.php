<?php
/*
 Plugin Name: VS Contributor interface
 Description: Enables contributors to upload files, manage recent posts and access help. 
 Version: 0.1
*/

// Create custom dashboard menu upon login
register_activation_hook( __FILE__, 'vs_contributor_activate' );

function vs_contributor_activate() {
  $menu_id = wp_create_nav_menu('Dashboard User Menu');
  //var_dump($menu_id);die;
  wp_update_nav_menu_item($menu_id, 0, array(
    'menu-item-title' => 'Help pages', 
    'menu-item-url' => 'http://mysite.com', 
    'menu-item-status' => 'publish')
  );

}  

// By default, contributors cant upload files - this changes that
add_action('admin_init', 'vs_contributor_uploads');
 
function vs_contributor_uploads() {
  if ( current_user_can('contributor') && !current_user_can('upload_files') ) { 
   	$contributor = get_role('contributor');
	$contributor->add_cap('upload_files');
  }	
}

// Give featured image box more prominence
// and move some dashboard boxes to right 
add_action('do_meta_boxes', 'vs_contributor_metaboxes');

function vs_contributor_metaboxes() {
    
    $screen = get_current_screen();
    //print $screen->id;
 
    // featured image
	remove_meta_box( 'postimagediv', 'post', 'side' );
	add_meta_box('postimagediv', __('Featured image'), 'post_thumbnail_meta_box', 'post', 'normal', 'high');
	
	if($screen->id == 'dashboard') {
	  // move drafts and pending to right
	  remove_meta_box( 'vs_contributor_draft', $screen->id, 'normal' );
	  add_meta_box('vs_contributor_draft', __('Recent draft posts'), 'vs_contributor_draft_output', $screen->id, 'side', 'high');
	
	  remove_meta_box( 'vs_contributor_pending', $screen->id, 'normal' );
	  add_meta_box('postimagediv', __('Recent pending posts'), 'vs_contributor_pending_output', $screen->id, 'side', 'high');
	}  

}

// Hook into the 'wp_dashboard_setup' action to register our other functions
add_action('wp_dashboard_setup', 'vs_contributor_dashboard_widgets' );

function vs_contributor_dashboard_widgets() {
	wp_add_dashboard_widget('vs_contributor_links', 'Help links', 'vs_contributor_links_output');
    wp_add_dashboard_widget('vs_contributor_draft', 'Recent draft posts', 'vs_contributor_draft_output');
	wp_add_dashboard_widget('vs_contributor_published', 'Recent published posts', 'vs_contributor_published_output');
	wp_add_dashboard_widget('vs_contributor_pending', 'Recent pending posts', 'vs_contributor_pending_output');	
} 

function vs_contributor_links_output() {
	wp_nav_menu( array('menu' => 'Dashboard User Menu' )); 
} 

function vs_contributor_draft_output() {
    vs_contributor_listposts('draft');
}

function vs_contributor_published_output() {
    vs_contributor_listposts('publish');
}

function vs_contributor_pending_output() {
    vs_contributor_listposts('pending');
}

function vs_contributor_listposts($status) {

	$query = new WP_Query( array(
			'post_type' => 'post',
			'post_status' => $status,
			'author' => $GLOBALS['current_user']->ID,
			'posts_per_page' => 5,
			'orderby' => 'modified',
			'order' => 'DESC'
    ) );
	$posts =& $query->posts;


    if ( $posts && is_array( $posts ) ) {
		$list = array();
		foreach ( $posts as $post ) {
		    $view_link = get_permalink( $post->ID ); 
			$edit_link = get_edit_post_link( $post->ID );
			$title = $post->post_title;
			$item = "<p><strong><a href='$edit_link' title='" . sprintf( __( 'Edit &#8220;%s&#8221;' ), esc_attr( $title ) ) . "'>" . esc_html($title) . "</a> <abbr title='" . get_the_time(__('Y/m/d g:i:s A'), $post) . "'>" . get_the_time( get_option( 'date_format' ), $post ) . '</abbr></strong>';
			if ( $the_content = preg_split( '#\s#', strip_tags( $post->post_content ), 11, PREG_SPLIT_NO_EMPTY ) )
				$item .= '<br />' . join( ' ', array_slice( $the_content, 0, 10 ) ) . ( 10 < count( $the_content ) ? '&hellip;' : '' ) . ' <a href="'.$view_link.'">View »</a> <a href="'.$edit_link.'">Edit »</a></p>';
			$list[] = $item;
		}
?>
	<ul>
		<li><?php echo join( "</li>\n<li>", $list ); ?></li>
	</ul>
	<p class="textright"><a href="edit.php?post_status=<?php echo $status ?>" class="button"><?php _e('View all'); ?></a></p>
<?php
	} else {
		_e('There are no posts in ' . $status . ' state at the moment');
	}
}








